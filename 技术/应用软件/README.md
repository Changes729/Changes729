# 其他应用软件

- [Marvelous Designer](https://www.marvelousdesigner.com/)：专业服装设计软件
- [Wireshark](https://www.wireshark.org/)
  - Linux 下免 sudo：`sudo usermod -a -G wireshark $USER`
  - Mac 下 软件会自动提示安装某应用程序。实现免 sudo。
- [jadx](https://github.com/skylot/jadx)：Java 反汇编工具
- minicom：
  - [发送换行](https://blog.csdn.net/qlexcel/article/details/111663373)：`ctrl+J (\n)`、`ctrl+M（\r）`
- FreeCAD
  - [A2plus](https://github.com/kbwbe/A2plus)：装配组件
- AutoSCAD
- Unity
- [fusion360](https://www.autodesk.com/products/fusion-360/overview?term=1-YEAR&tab=subscription)：机械机构设计。
- **[lyrebird](https://github.com/lyrebird-voice-changer/lyrebird)**：变音软件
- AVAHI：零配置mDNS/DNS工具

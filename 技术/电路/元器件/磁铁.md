# 磁铁

圆柱形磁铁，有些是轴向磁化的，有些是周向磁化的。使用在编码器内的磁铁需要是周向磁化的，这样圆形磁铁，切一半，半圆一边是N，一边是S。

- 如何区分周向磁化和轴向磁化？

  无法进行周向转动，那么就是周向磁化（径向磁化）。



## 供应商

- [上海磁铁批发中心](https://item.taobao.com/item.htm?spm=a1z10.5-c.w4002-15813367632.13.363a6319hMstzh&id=548217004909)
  - [轴向充磁](https://item.taobao.com/item.htm?spm=a1z10.5-c.w4002-15813367632.13.363a6319hMstzh&id=548217004909)
- [lalaci拉拉磁铁](https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-6377071317.9.177f55b1xmP82h&id=541322088764)
  - [径向充磁](https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-6377071317.9.177f55b1xmP82h&id=541322088764)
- [6*1.5](https://item.taobao.com/item.htm?_u=l2d3uchq8dac&id=584979410613&skuId=4988045642158&spm=a1z09.2.0.0.3fd82e8dueS12k)


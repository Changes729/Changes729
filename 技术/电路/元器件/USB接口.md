> 参考资料：
>
> - [免外围电路的单片机串口一键下载方案](https://www.wch.cn/application/575.html)

# USB 接口

- [USB TypeA 直插90度母座 ](https://item.taobao.com/item.htm?spm=a1z09.2.0.0.39ab2e8dZ5Z6Lr&id=4797812435&_u=n2d3uchq48e0) —— Kicad USB_A_Molex_67643_Horizontal

  ![](https://gd3.alicdn.com/imgextra/i3/12762090/O1CN01BNrzFX1RJGR9GdYVH_!!12762090.jpg_400x400.jpg)

- [USB TypeC 16P 母座](https://item.taobao.com/item.htm?spm=a1z09.2.0.0.39ab2e8dZ5Z6Lr&id=4797812435&_u=n2d3uchq48e0) —— Kicad USB_C_Receptacle_XKB_U262-16XN-4BVC11

  ![](https://gd2.alicdn.com/imgextra/i3/12762090/O1CN01YT3bgz1RJGRDP48dh_!!12762090.jpg_400x400.jpg)
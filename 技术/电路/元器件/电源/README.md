> 参考资料：
>
> - [电池研究院：电池分类与命名大全](https://www.pcauto.com.cn/tech/2368/23689092.html)
> - [常用1.5V圆柱型电池在中文圈的代号对照](https://zh.wikipedia.org/wiki/%E9%9B%BB%E6%B1%A0%E5%B0%BA%E5%AF%B8%E5%88%97%E8%A1%A8)

# 电源

- [LDO](https://en.wikipedia.org/wiki/Low-dropout_regulator)（low-dropout regulator，低电压降电压调整器）
- VCC (Circuit)
- VSS (Serial)
- VDD (Device)



## 电池类型

- 根据储能材料分类
  - 磷酸铁锂-锂离子电池
  - 碳锌锰干电池（碱性电池、一次性电池）
- 根据形状来分
  - 圆柱电池
  - 方形电池
  - 纽扣电池



18650锂电池可充电，其外径18mm，长度65。和AA电池（5号电池，外径14.5mm，长度50,14500）、AAA电池（7号电池，10.5外径，44.5长度，10440）都不一样



## 充电电路

- [CN3781](https://www.bilibili.com/video/BV1DC4y1p7uW/?spm_id_from=333.337.search-card.all.click&vd_source=b736aa3d7f0fdf47b59ea3021dc810ab)：4A锂电池充电管理芯片
- [TP5100](https://www.bilibili.com/video/BV1S5411t79J/?spm_id_from=333.788)：2s锂电池充电管理芯片，通过引脚CS选择单芯电池或多芯电池。
- [SY6912](https://www.bilibili.com/video/BV1HK4y1k7Dq/?spm_id_from=333.788&vd_source=b736aa3d7f0fdf47b59ea3021dc810ab)：3s锂电池充电，[淘宝](https://item.taobao.com/item.htm?spm=a230r.1.14.16.447e4a4dSe8bs0&id=653995640130&ns=1&abbucket=10#detail)
- [锂电池保护](https://detail.tmall.com/item.htm?spm=a230r.1.14.38.5a7f959dfTi95x&id=558346048387&ns=1&abbucket=10)：这种是被动式平衡充，一般是充电IC+锂电池保护板配合
- [AMS1117](https://so.szlcsc.com/global.html?k=AMS1117&hot-key=)：LDO 三端稳压芯片


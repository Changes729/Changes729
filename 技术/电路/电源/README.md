> 参考资料：
>
> - [【MOS管】防反接电路](https://www.bilibili.com/video/BV1tY4y1P7Kp)：二极管防反接、P沟道MOS管防反接、N沟道MOS管防反接（NCE4060K）
> - [安规电容（X/Y电容）](https://zhuanlan.zhihu.com/p/350438523)

# 电源

## 电池

- [聚合物锂电池](https://en.wikipedia.org/wiki/Lithium-ion_battery#Safety)：[爆炸实验](https://www.youtube.com/watch?v=eZxDC-whz14)
  
  > 供应商：
  >
  > - [裕辉达玩具电池配件商](https://shop505073405.taobao.com/)
  > - [洛其新能源](https://item.taobao.com/item.htm?spm=a230r.1.14.39.52fb5825d22anJ&id=528453120288&ns=1&abbucket=7#detail)
  > - [广州市莱悦电子科技](https://shop126557863.taobao.com/)
  
  聚合物锂电池的内阻低，质量轻，深受航模、小型电子产品所喜爱。然而，其因穿刺、弯折、过充放电会导致电池鼓包，严重的会导致燃烧。
  
  安全工作电压范围在 3.3v ~ 4.2v 之间。充电会充到 3.8v 进行保存。
  
  标准充电方法：0.5C CC（恒流）充电至4.25V，再CV（恒压4.2V）充电直至充电电流≤0.05C
  



## 无线充电

- [XKT-412/XKT-335/XKT-001](https://cb-electronics.com/products/xkt-335/)



## 智能插座（可控电源）

### 公牛

### gosund（小米供应链）

### 华为（正泰代工）

### 向日葵


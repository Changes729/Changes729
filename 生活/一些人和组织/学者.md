# 学者和组织记录

- [杨东平](http://rdbk1.ynlib.cn:6251/qw?s0=4&s1=杨东平)：杨老师对教育机制的文章让我感受到他对教育事业的热情。
- [温铁军](https://kns.cnki.net/kcms2/author/detail?v=7P_nOixU6lUPgaPkiZNsGX3SC2wEjRGfFZP4p9Oy3AqmrszurHeyRToVTa7A7Wvg8Y_7ESCerGyEuf-24ESaBClPyuumxgKZnndCWR4lTS2V9m-gYARXWzBElYcqblF5&uniplatform=NZKPT&language=CHS)：中国人名大学，农业经济;经济体制改革;宏观经济管理与可持续发展；
- [周子书](https://kns.cnki.net/kcms2/author/detail?v=7P_nOixU6lVCNbmzvfq27o-5nSuO-B4aLYuRYMNy3QTDq6FrMWn4uLE2EpQYuBxBCT7GwzJBzjgKUrfF5P2Xhnuy1U5zfUobEdOKbA125U9c8VqNuiOYMBuH1AAVTFJD&uniplatform=NZKPT&language=CHS)：中央美术学院，美术书法雕塑与摄影;建筑科学与工程;中国政治与国际政治;
- [贺雪峰](https://kns.cnki.net/kcms2/author/detail?v=7P_nOixU6lXFI34kdjkoL2xwlYBptXHt-0BEFzwbJf1h2xvH_gJA_06TyLgWfCWhHE5QuBUarSCuOW2KWvmzJOPMAklddoHZJOCmjCxoykV_P81YtmSJBTZJcVQPtzc3&uniplatform=NZKPT&language=CHS)：武汉大学，农业经济;政党及群众组织;社会学及统计学;
- [高飞](http://zju-fast.com/fei-gao/)：浙江大学，FAST Lab 无人机团队
- [王冠云](https://design.zju.edu.cn/guanyunlab/members)：浙江大学，[智慧形态实验室](https://design.zju.edu.cn/guanyunlab/)



- [上海创新创意设计研究院（DIIS）](https://diis.org.cn/Cn)：公众号「设计寰宇」。重点是教学和学习方法的教育创新。
- [爱故乡](https://www.aiguxiang.com.cn/)：“爱故乡”品牌致力于文创赋能乡村振兴，注重人才建设，开展“文创+”项目，打造小毛驴综合体等。